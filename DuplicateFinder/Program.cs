﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DublicateFinder
{
    class CompareFile
    {
        public readonly string FullName;
        public readonly string Name;
        public readonly long Length;
        public CompareFile(string fullName, string name, long length)
        {
            FullName = fullName;
            Name = name;
            Length = length;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string testFilesPath = "";
            if (args.Length > 0)
            {
                testFilesPath = args[0];
            }
            else
            {
                string startupPath = Environment.CurrentDirectory;
                testFilesPath = @"D:\Download";
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var filesInfo = new DirectoryInfo(testFilesPath).GetFiles("*.*", SearchOption.AllDirectories);

            var resultDictionary = new Dictionary<string, List<string>>();

            stopWatch.Start();
            var comparingFiles = filesInfo.Select(f => new CompareFile(f.FullName, f.Name, f.Length)).GroupBy(f => f.Length).
                ToDictionary(group => group.Key, group => group.ToList()).OrderBy(f => f.Key).Where(x => x.Value.Count > 1 && x.Key > 0);
            
            foreach(var comparingFile in comparingFiles)
            {
                CompareFiles(comparingFile.Value, resultDictionary);
            }

            stopWatch.Stop();

            foreach(var result in resultDictionary)
            {
                string path = result.Key.Substring(0, result.Key.LastIndexOf(" : "));
                string fileName = Path.GetFileName(path);
                Console.WriteLine(fileName);

                foreach(var dupFile in result.Value)
                {
                    Console.WriteLine('\t' + dupFile);
                }
            }

            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = $"{ts.Hours}.{ts.Minutes}:{ts.Seconds}:{ts.Milliseconds / 10}";
            Console.WriteLine("RunTime " + ((int)ts.TotalMilliseconds).ToString());

            Console.ReadLine();
        }

        static void CompareFiles(List<CompareFile> files, Dictionary<string, List<string>> result)
        {
            for (int i = 0; i < files.Count - 1; i++)
            {
                for (int j = i + 1; j < files.Count; j++)
                {
                    using (var FirstFile = new FileStream(files[i].FullName, FileMode.Open, FileAccess.Read))
                    {
                        using (var SecondFile = new FileStream(files[j].FullName, FileMode.Open, FileAccess.Read))
                        {
                            byte[] byte1 = null;
                            byte[] byte2 = null;

                            if (FirstFile.Length < 1024)
                            {
                                byte1 = byte2 = new byte[FirstFile.Length];
                            }
                            else
                            {
                                byte1 = byte2 = new byte[1024];
                            }

                            int res1, res2;
                            bool processBytesComparing = true;
                            do
                            {
                                res1 = FirstFile.Read(byte1, 0, byte1.Length);
                                res2 = SecondFile.Read(byte2, 0, byte2.Length);

                                for (int q = 0; q < byte1.Length; q++)
                                {
                                    if (byte1[q] != byte2[q])
                                    {
                                        goto skipAdding;
                                    }
                                }
                                
                                if(res1 != 0 && res2 != 0)
                                {
                                    processBytesComparing = false;
                                }
                            }
                            while (processBytesComparing);

                            string key = files[i].FullName + " : " + files[i].Length;
                            if (!result.TryGetValue(key, out List<string> values))
                            {
                                values = new List<string>() { files[i].FullName, files[j].FullName };
                                result.Add(key, values);
                            }
                            else
                            {
                                values.Add(files[j].FullName);
                            }

                            files.RemoveAt(j);
                            j--;

                            skipAdding:;
                        }
                    }
                }
            }
        }
    }
}
